#!/usr/bin/env python3


from users.models import Contributor
import yaml


# Create initial contributors data

with open('data/contributors.yml') as f:
    contributors = yaml.safe_load(f)

for contributor in contributors:
    try:
        user = Contributor.objects.get_or_create(
            username = contributor['username'],
            email = contributor['email'],
            password = contributor['password'],
            is_active = contributor['is_active'],
            is_staff = contributor['is_staff'],
            is_superuser = contributor['is_superuser']
        )
    except:
        pass
