from django.test import TestCase
from users.models import Contributor



"""
TestCase is going to create a temporary clean database
which will be detroyed after the unit test stops
"""

class TestConfig(TestCase):
    """ Create a user object that we'll use during the test """

    @classmethod
    def setUpTestData(cls): 
        Contributor.objects.create_superuser(
            email = 'randomsuperuser@gmail.com', 
            username = 'randomsuperuser'
        )
        Contributor.objects.create_user(
            email = 'randomuser@gmail.com',
            username = 'randomuser'
        )


class ContributorTest(TestConfig): 

    def test_email_label(self):
        contributor = Contributor.objects.get(id = 1)
        field_label = contributor._meta.get_field('email').verbose_name
        self.assertEqual(field_label, 'email')

    def test_email_max_length(self):
        contributor = Contributor.objects.get(id = 1)
        max_length = contributor._meta.get_field('email').max_length
        self.assertEqual(max_length, 255)

    def test_username_label(self):
        contributor = Contributor.objects.get(id = 1)
        field_label = contributor._meta.get_field('username').verbose_name
        self.assertEqual(field_label, 'username')

    def test_username_max_length(self):
        contributor = Contributor.objects.get(id = 1)
        max_length = contributor._meta.get_field('username').max_length
        self.assertEqual(max_length, 150)

    def test_superuser_is_active(self):
        contributor = Contributor.objects.get(id = 1)
        user_status = contributor.is_active
        self.assertEqual(user_status, True)

    def test_user_is_active(self):
        contributor = Contributor.objects.get(id = 2)
        user_status = contributor.is_active
        self.assertEqual(user_status, True)

    def test_superuser_is_staff(self):
        contributor = Contributor.objects.get(id = 1)
        user_status = contributor.is_staff 
        self.assertEqual(user_status, True)

    def test_user_is_staff(self):
        contributor = Contributor.objects.get(id = 2)
        user_status = contributor.is_staff 
        self.assertEqual(user_status, False)

    def test_superuser_is_superuser(self):
        contributor = Contributor.objects.get(id = 1)
        user_status = contributor.is_superuser 
        self.assertEqual(user_status, True)

    def test_user_is_superuser(self):
        contributor = Contributor.objects.get(id = 2)
        user_status = contributor.is_staff 
        self.assertEqual(user_status, False)

    def test_string_representaion(self):
        contributor = Contributor.objects.get(id = 1)
        exp_representation = f'{contributor.username}'
        self.assertEqual(str(contributor), exp_representation)

    def test_verbose_name_plural(self):
        object_plural = Contributor._meta.verbose_name_plural
        self.assertEqual(object_plural, 'users')