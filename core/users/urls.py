from django.urls import path, include

from .views import ContributorList, RegisterView, ResetPasswordRequest, activate_reset



app_name = 'users'

urlpatterns = [
    path('', include('dj_rest_auth.urls')), 
    path('contributors/', ContributorList.as_view(), name = 'contributors-list'),
    path('register/', RegisterView.as_view(), name = 'register'),
    path('password-reset-request/', ResetPasswordRequest.as_view(), name = 'password-reset-request'),
    path('password-reset/<uidb64>/<token>/', activate_reset, name = 'password-reset-activate'),
]