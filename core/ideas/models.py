from django.db import models
from django.utils import timezone
from users.models import Contributor
from tags.models import Tag



class Idea(models.Model):
    """ Store information on project ideas and plans"""

    contributors = models.ManyToManyField(Contributor)
    tags = models.ManyToManyField(Tag)
    name = models.CharField(max_length = 300)
    draft = models.TextField(blank = False, default = 'none')
    description = models.TextField(blank = False, default = 'none')
    date_created = models.DateTimeField(default = timezone.now)

    def __str__(self):
        return self.name 


class Task(models.Model):
    """ Store information about tasks for implementing a project idea (Idea)"""

    idea = models.ForeignKey(Idea, on_delete = models.CASCADE)
    name = models.TextField()

    def __str__(self):
        return self.name
