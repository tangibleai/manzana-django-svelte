from django.contrib import admin

from .models import Idea, Task



admin.site.register(Idea)
admin.site.register(Task)
