from rest_framework import generics
from .models import Idea
from .serializers import IdeaSerializer



class ListIdea(generics.ListAPIView):

    queryset = Idea.objects.all()
    serializer_class = IdeaSerializer


class DetailIdea(generics.RetrieveUpdateAPIView):

    queryset = Idea.objects.all()
    serializer_class = IdeaSerializer
