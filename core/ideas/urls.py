from django.urls import path
from .views import ListIdea, DetailIdea



app_name = 'ideas'

urlpatterns = [
    path('', ListIdea.as_view(), name = 'list-ideas'),
    path('<int:pk>/', DetailIdea.as_view(), name = 'idea-detail'),
]
